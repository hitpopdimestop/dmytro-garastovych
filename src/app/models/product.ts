export interface ProductInterface {
  _id: string;
  index: number;
  sale: boolean;
  price: number;
  rating: number;
  brand: string;
  picture: string;
  color: string;
  name: string;
}
