export interface ColorInterface {
  name: string;
  rgb: number[];
}

export interface BrandInterface {
  name: string;
  logo: string;
}

export interface ConfigInterface {
  colors: ColorInterface[];
  brands: BrandInterface[];
}
