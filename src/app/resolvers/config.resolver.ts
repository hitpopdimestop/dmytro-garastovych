import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { ApiService } from '../services/api/api.service';

@Injectable({
  providedIn: 'root'
})
export class ConfigResolver implements Resolve<ConfigResolver> {

  constructor(private api: ApiService, private router: Router) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<ConfigResolver> {
    return this.api.getConfig().pipe(
      catchError(error => this.handleError(error)),
      tap(console.log)
    );
  }

  private handleError(error) {
    console.log(error);
    this.router.navigate(['/404']);
    return of(null);
  }

}
