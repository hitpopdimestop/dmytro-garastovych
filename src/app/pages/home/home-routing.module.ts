import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeIndexComponent } from './components/home-index/home-index.component';
import { ConfigResolver } from '../../resolvers/config.resolver';

const routes: Routes = [
  {
    path: '',
    component: HomeIndexComponent,
    resolve: {
      config: ConfigResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
