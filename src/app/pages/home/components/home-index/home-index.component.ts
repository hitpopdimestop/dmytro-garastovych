import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConfigInterface } from '../../../../models/config';

@Component({
  selector: 'app-home-index',
  templateUrl: './home-index.component.html',
  styleUrls: ['./home-index.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeIndexComponent implements OnInit {

  config: ConfigInterface;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.config = this.route.snapshot.data.config;
  }

}
