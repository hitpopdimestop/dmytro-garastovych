import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShopIndexComponent } from './components/shop-index/shop-index.component';
import { ConfigResolver } from '../../resolvers/config.resolver';
import { ProductsResolver } from './resolvers/products.resolver';

const routes: Routes = [
  {
    path: '',
    component: ShopIndexComponent,
    resolve: {
      config: ConfigResolver,
      products: ProductsResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopRoutingModule { }
