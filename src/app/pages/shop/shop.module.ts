import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShopIndexComponent } from './components/shop-index/shop-index.component';
import { ShopRoutingModule } from './shop-routing.module';
import { ProductsResolver } from './resolvers/products.resolver';
import { ProductComponent } from './components/product/product.component';
import { MobxAngularModule } from 'mobx-angular';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [ShopIndexComponent, ProductComponent],
  imports: [
    CommonModule,
    ShopRoutingModule,
    MobxAngularModule,
    FormsModule
  ],
  providers: [
    ProductsResolver
  ]
})
export class ShopModule { }
