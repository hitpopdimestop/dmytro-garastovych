import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductInterface } from '../../../../models/product';
import { BrandInterface, ColorInterface, ConfigInterface } from '../../../../models/config';
import { action, computed, observable } from 'mobx-angular';

@Component({
  selector: 'app-shop-index',
  templateUrl: './shop-index.component.html',
  styleUrls: ['./shop-index.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShopIndexComponent implements OnInit {

  @observable allProducts: ProductInterface[];
  config: ConfigInterface;

  @observable itemsToShow = 10;
  @observable minPrice: number;
  @observable maxPrice: number;
  @observable name: string;
  @observable onSale: boolean;
  colors: ColorInterface[];
  brands: BrandInterface[];

  constructor(private route: ActivatedRoute) {
  }

  @action showMore(): void {
    this.itemsToShow = this.itemsToShow + 10;
  }

  @action resetAll(): void {
    this.minPrice = null;
    this.maxPrice = null;
    this.name = null;
  }

  @computed get getFilteredProducts(): ProductInterface[] {
    return this.allProducts
      .filter(product => {
        if (this.minPrice) {
          return product.price >= this.minPrice;
        } else {
          return true;
        }
      })
      .filter(product => {
        if (this.maxPrice) {
          return product.price <= this.maxPrice;
        } else {
          return true;
        }
      })
      .filter(product => {
        if (this.name) {
          return product.name.toLowerCase().match(this.name.toLowerCase());
        } else {
          return true;
        }
      })
      .filter(product => {
        if (this.onSale) {
          return product.sale;
        } else {
          return true;
        }
      });
  }

  @computed get items(): ProductInterface[] {
    return this.getFilteredProducts.slice(0, this.itemsToShow);
  }

  @computed get availableMore(): boolean {
    return this.allProducts.length > this.itemsToShow;
  }

  ngOnInit() {
    this.allProducts = this.route.snapshot.data.products;
    this.config = this.route.snapshot.data.config;
    if (this.route.snapshot.queryParams.brand) {

    }
  }

}
