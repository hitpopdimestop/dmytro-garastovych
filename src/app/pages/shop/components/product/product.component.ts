import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { ProductInterface } from '../../../../models/product';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductComponent implements OnInit {

  @Input() product: ProductInterface;

  constructor() { }

  ngOnInit() {
  }

}
