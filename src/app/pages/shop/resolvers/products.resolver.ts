import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { ProductInterface } from '../../../models/product';
import { ApiService } from '../../../services/api/api.service';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable()
export class ProductsResolver implements Resolve<ProductInterface[]> {

  constructor(private api: ApiService, private router: Router) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<ProductInterface[]> {
    return this.api.getProducts().pipe(
      catchError(error => this.handleError(error)),
      tap(console.log)
    );
  }

  private handleError(error) {
    console.log(error);
    this.router.navigate(['/404']);
    return of(null);
  }

}
