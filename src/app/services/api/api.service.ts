import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigInterface } from '../../models/config';
import { Observable } from 'rxjs';
import { ProductInterface } from '../../models/product';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getConfig(): Observable<ConfigInterface> {
    return this.http.get<ConfigInterface>('https://us-central1-epam-hiring-event.cloudfunctions.net/options');
  }

  getProducts(): Observable<ProductInterface[]> {
    return this.http.get<ProductInterface[]>('https://us-central1-epam-hiring-event.cloudfunctions.net/list');
  }

}
